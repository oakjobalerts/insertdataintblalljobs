package com.model;

public class FeedOrderModel {

    String feedName;
    String sourceNumber;
    String active;
    String removeDuplicate;

    public String getRemoveDuplicate() {
        return removeDuplicate;
    }

    public void setRemoveDuplicate(String removeDuplicate) {
        this.removeDuplicate = removeDuplicate;
    }

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    public String getSourceNumber() {
        return sourceNumber;
    }

    public void setSourceNumber(String sourceNumber) {
        this.sourceNumber = sourceNumber;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public FeedOrderModel() {
        super();
    }

    public FeedOrderModel(String feedName, String sourceNumber, String active, String removeDuplicate) {
        super();
        this.feedName = feedName;
        this.sourceNumber = sourceNumber;
        this.active = active;
        this.removeDuplicate = removeDuplicate;
    }

}
