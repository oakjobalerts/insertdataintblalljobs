package com.model;

import java.util.List;

public class DisplayPhpMapping {

    List<String> source;
    List<String> sourceInsert;

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    public List<String> getSourceInsert() {
        return sourceInsert;
    }

    public void setSourceInsert(List<String> sourceInsert) {
        this.sourceInsert = sourceInsert;
    }

    public DisplayPhpMapping() {
        super();
    }

    public DisplayPhpMapping(List<String> source, List<String> sourceInsert) {
        super();
        this.source = source;
        this.sourceInsert = sourceInsert;
    }

}
