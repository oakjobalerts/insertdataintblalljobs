package com.model;

public class phpMappingClassModel {

    String source;
    String sourceInsert;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceInsert() {
        return sourceInsert;
    }

    public void setSourceInsert(String sourceInsert) {
        this.sourceInsert = sourceInsert;
    }

    public phpMappingClassModel(String source, String sourceInsert) {
        super();
        this.source = source;
        this.sourceInsert = sourceInsert;
    }

    public phpMappingClassModel() {
        super();
    }

}
