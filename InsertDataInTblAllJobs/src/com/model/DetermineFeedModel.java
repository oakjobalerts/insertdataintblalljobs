package com.model;

public class DetermineFeedModel implements Comparable<DetermineFeedModel> {

    String feedName;
    String feedOrder;
    String status;
    String maxJobs;
    String index;
    String type;
    String removeDublicate;
    String backFill;


    String cpcInFeed;
    String isInFeedCpcAvailable;

    float effectiveCpc;
    String grossCpc;
    String salesCommission1;
    String salesCommission2;
    String clickDiffAdjestment;
    String discardedClicks;
    String avg7dayCpc;

    public String getDiscardedClicks() {
        return discardedClicks;
    }

    public void setDiscardedClicks(String discardedClicks) {
        this.discardedClicks = discardedClicks;
    }

    public String getBackFill() {
        return backFill;
    }

    public void setBackFill(String backFill) {
        this.backFill = backFill;
    }

    public String getRemoveDublicate() {
        return removeDublicate;
    }

    public String getIsInFeedCpcAvailable() {
        return isInFeedCpcAvailable;
    }

    public String getCpcInFeed() {
        return cpcInFeed;
    }

    public void setCpcInFeed(String cpcInFeed) {
        this.cpcInFeed = cpcInFeed;
    }

    public void setIsInFeedCpcAvailable(String isInFeedCpcAvailable) {
        this.isInFeedCpcAvailable = isInFeedCpcAvailable;
    }

    public void setRemoveDublicate(String removeDublicate) {
        this.removeDublicate = removeDublicate;
    }

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    public String getFeedOrder() {
        return feedOrder;
    }

    public void setFeedOrder(String feedOrder) {
        this.feedOrder = feedOrder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMaxJobs() {
        return maxJobs;
    }

    public void setMaxJobs(String maxJobs) {
        this.maxJobs = maxJobs;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getEffectiveCpc() {
        return effectiveCpc;
    }

    public void setEffectiveCpc(float effectiveCpc) {
        this.effectiveCpc = effectiveCpc;
    }

    public String getGrossCpc() {
        return grossCpc;
    }

    public void setGrossCpc(String grossCpc) {
        this.grossCpc = grossCpc;
    }

    public String getSalesCommission1() {
        return salesCommission1;
    }

    public void setSalesCommission1(String salesCommission1) {
        this.salesCommission1 = salesCommission1;
    }

    public String getSalesCommission2() {
        return salesCommission2;
    }

    public void setSalesCommission2(String salesCommission2) {
        this.salesCommission2 = salesCommission2;
    }

    public String getClickDiffAdjestment() {
        return clickDiffAdjestment;
    }

    public void setClickDiffAdjestment(String clickDiffAdjestment) {
        this.clickDiffAdjestment = clickDiffAdjestment;
    }

    public String getAvg7dayCpc() {
        return avg7dayCpc;
    }

    public void setAvg7dayCpc(String avg7dayCpc) {
        this.avg7dayCpc = avg7dayCpc;
    }

    public DetermineFeedModel() {
        super();
    }

    @Override
    public int compareTo(DetermineFeedModel o) {

        float feedOrder = this.getEffectiveCpc();
        float oFeed = o.getEffectiveCpc();
        if (feedOrder >= oFeed) {
            return -1;
        } else if (feedOrder < oFeed) {
            return 1;
        }
        return 0;
    }

}
