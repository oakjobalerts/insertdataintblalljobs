package com.model;

public class FeedSourceQueriesModel {

    String feedName;
    String query;

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public FeedSourceQueriesModel(String feedName, String query) {
        super();
        this.feedName = feedName;
        this.query = query;
    }

    public FeedSourceQueriesModel() {
        super();
    }

}
