package com.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.model.DetermineFeedModel;
import com.model.DisplayPhpMapping;
import com.model.FeedSourceQueriesModel;
import com.model.phpMappingClassModel;
import com.sendgrid.SendGrid;

public class NewFeedOrderDedupeMain {

	// Local test
	// public static String masterFilePath =
	// "/home/signity/Desktop/pawan_data/MasterFeedOrder.txt";
	// public static String pathToWriteFile =
	// "/home/signity/Desktop/pawan_data/databaseSourceMappingFile.txt";
	// public static String jsonPhpFilePath =
	// "/home/signity/Desktop/pawan_data/sourceMappingJson.json";
	// public static String readSourceFeedQueryFilePath =
	// "/home/signity/Desktop/pawan_data/readFeedSourceQueries.txt";
	// public static String readSourceDisplayMappingFilePath =
	// "/home/signity/Desktop/pawan_data/readSourceMappingJson.txt";

	// Server path

	public static String masterFilePath =
			"/var/nfs-93/redirect/mis_logs/DeterminedFeedOrder/MasterFeedOrder.txt";
	public static String pathToWriteFile =
			"/var/nfs-93/redirect/mis_logs/masterSourceMapping/databaseSourceMappingFile.txt";
	public static String jsonPhpFilePath =
			"/var/nfs-93/redirect/mis_logs/masterSourceMapping/sourceMappingJson.json";
	public static String readSourceFeedQueryFilePath =
			"/var/nfs-93/redirect/mis_logs/masterSourceMapping/readFeedSourceQueries.txt";
	public static String readSourceDisplayMappingFilePath = "/var/nfs-93/redirect/mis_logs/masterSourceMapping/readSourceMappingJson.txt";

	public static Map<String, String> tableMap;
	public static Map<String, String> sourceMappingMap;

	public static String viktreCareerFeedName = "Viktre Careers";

	public static void main(String[] args) {

		boolean isPortfolium = false;

		try {
			if (args[0].equals("portfolium")) {
				isPortfolium = true;
			}

		} catch (Exception e) {
		}

		List<DetermineFeedModel> determinedFeedList = readMasterFeedOrder();
		determinedFeedList = removeApiFromFeedOrder(determinedFeedList);
		setUpTableMap();

		prepareQuery(determinedFeedList, isPortfolium);
		if (!isPortfolium) {
			writeFilesOnServer(determinedFeedList);
		}
	}

	public static List<DetermineFeedModel> readMasterFeedOrder() {

		BufferedReader bufferedReader = null;
		List<DetermineFeedModel> determineFeedModelList = new ArrayList<DetermineFeedModel>();
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		try {
			bufferedReader = new BufferedReader(new FileReader(masterFilePath));
			determineFeedModelList = gson.fromJson(bufferedReader, new TypeToken<List<DetermineFeedModel>>() {
			}.getType());
		} catch (Exception e) {
			// e.printStackTrace();
		} finally {
			try {
				bufferedReader.close();
			} catch (IOException e) {
				// e.printStackTrace();
			}
		}
		return determineFeedModelList;
	}

	public static List<DetermineFeedModel> removeApiFromFeedOrder(List<DetermineFeedModel> determinedFeedOrderList) {

		List<DetermineFeedModel> updatedDeterminedFeedOrderList = new ArrayList<DetermineFeedModel>();
		for (DetermineFeedModel model : determinedFeedOrderList) {
			if (model.getType().toLowerCase().equals("api") || model.getFeedName().toLowerCase().contains("api")) {
				continue;
			}
			updatedDeterminedFeedOrderList.add(model);
		}
		return updatedDeterminedFeedOrderList;

	}

	// This method will write sourceMappingJson.json and
	// databaseSourceMappingFile.txt to be read by queue making and php team.
	public static void writeFilesOnServer(List<DetermineFeedModel> feedOrderList) {

		List<String> source = new ArrayList<String>();
		List<String> sourceInsert = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			source.add("");
			sourceInsert.add("");
		}

		// Write File for Query mapping java program
		try {
			String fileWriteString = "";
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(pathToWriteFile));

			for (DetermineFeedModel feed : feedOrderList) {
				fileWriteString = fileWriteString + feed.getFeedName() + "|" + feed.getFeedOrder() + "|" + sourceMappingMap.get(feed.getFeedName()) + "\n";
			}
			bufferedWriter.write(fileWriteString);
			bufferedWriter.close();
		} catch (Exception e) {
			// sendExceptionEmail("High Alert Exception in jobs uploading",
			// "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>"
			// + e);
			// e.printStackTrace();
		}

		// Write JSON file to be read by Php Team
		source.add("");
		sourceInsert.add("");
		for (DetermineFeedModel feed : feedOrderList) {
			try {
				if (sourceMappingMap.get(feed.getFeedName()).trim().equals("-1") == true) {
					source.set(Integer.parseInt(feed.getFeedOrder()), "");
				} else
					source.set(Integer.parseInt(feed.getFeedOrder()), sourceMappingMap.get(feed.getFeedName()).trim());
				sourceInsert.set(Integer.parseInt(feed.getFeedOrder()), feed.getFeedName().trim());

			} catch (Exception e) {
				// sendExceptionEmail("High Alert Exception in jobs uploading",
				// "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>"
				// + e);
				// e.printStackTrace();
			}
		}

		DisplayPhpMapping displayMapiing = new DisplayPhpMapping();
		displayMapiing.setSource(source);
		displayMapiing.setSourceInsert(sourceInsert);
		String json = new Gson().toJson(displayMapiing);

		try {
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(jsonPhpFilePath));
			bufferedWriter.write(json);
			bufferedWriter.close();
		} catch (Exception e) {
			sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
			// e.printStackTrace();
		}

	}

	// This method will read readSourceMappingJson and readFeedSourceQueries for
	// mapping and queries of feed order sources
	public static void setUpTableMap() {
		sourceMappingMap = new HashMap<String, String>();
		tableMap = new HashMap<String, String>();
		try {
			// Read mapping file
			BufferedReader bufferedReader = new BufferedReader(new FileReader(readSourceDisplayMappingFilePath));
			Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
			List<phpMappingClassModel> phpMappingList = new ArrayList<phpMappingClassModel>();
			phpMappingList = gson.fromJson(bufferedReader, new TypeToken<List<phpMappingClassModel>>() {
			}.getType());
			bufferedReader.close();
			for (phpMappingClassModel phpMapping : phpMappingList) {
				sourceMappingMap.put(phpMapping.getSource(), phpMapping.getSourceInsert());
			}

			// Read queries files
			BufferedReader bufferedReader2 = new BufferedReader(new FileReader(readSourceFeedQueryFilePath));
			List<FeedSourceQueriesModel> feedSourceQueryList = new ArrayList<FeedSourceQueriesModel>();
			feedSourceQueryList = gson.fromJson(bufferedReader2, new TypeToken<List<FeedSourceQueriesModel>>() {
			}.getType());
			bufferedReader2.close();
			for (FeedSourceQueriesModel sourceQuery : feedSourceQueryList) {
				tableMap.put(sourceQuery.getFeedName(), sourceQuery.getQuery());
			}
		} catch (Exception e) {
			// TODO Uncomment
			// sendExceptionEmail("High Alert Exception in jobs uploading",
			// "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>"
			// + e);
			// e.printStackTrace();
		}
	}

	public static void prepareQuery(List<DetermineFeedModel> determineFeedModelList, boolean isPortfolium) {
		String query = "";
		String portfoliumQuery = "";

		// Prepare feed with duplicate jobs removal active
		for (DetermineFeedModel feed : determineFeedModelList) {

			if (feed.getStatus().equals("1") && !feed.getFeedName().contains("Api") && feed.getRemoveDublicate().equals("1")) {
				try {
					feed = removeEmptyOrNullRateParameters(feed);
					// IF CPC BLOCK
					if (feed.getIsInFeedCpcAvailable().equals("1") && feed.getCpcInFeed().equals("1")) {
						String queryString = createInFeedCpcQuery(feed);
						query = query + queryString;

						portfoliumQuery = portfoliumQuery + createInFeedCpcQueryForPortfolium(feed, isPortfolium);
					}
					// FOR CPC BLOCK
					else {
						String queryString = createFixedRateCpcFeedQuery(feed);
						query = query + queryString;

						portfoliumQuery = portfoliumQuery + createFixedRateCpcFeedQueryForPortfolium(feed, isPortfolium);
					}

				} catch (Exception e) {

					// TODO Uncomment
					// sendExceptionEmail("High Alert Exception in jobs uploading",
					// "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>"
					// + e);
					// e.printStackTrace();
				}
			}
		}
		// Alter table add unique index, and later remove unique index
		query = query + "ALTER IGNORE TABLE tbl_all_jobs ADD UNIQUE INDEX idx_name (employer, title, zipcode);"
				+ "ALTER TABLE tbl_all_jobs drop INDEX idx_name;";

		portfoliumQuery = portfoliumQuery + "ALTER IGNORE TABLE tbl_all_jobs ADD UNIQUE INDEX idx_name (employer, title, zipcode);"
				+ "ALTER TABLE tbl_all_jobs drop INDEX idx_name;";

		for (DetermineFeedModel feed : determineFeedModelList) {
			if (feed.getStatus().equals("1") && !feed.getFeedName().contains("Api") && feed.getRemoveDublicate().equals("0")) {
				try {

					feed = removeEmptyOrNullRateParameters(feed);
					// IF CPC BLOCK
					if (feed.getIsInFeedCpcAvailable().equals("1") && feed.getCpcInFeed().equals("1")) {
						String queryString = createInFeedCpcQuery(feed);
						query = query + queryString;

						portfoliumQuery = portfoliumQuery + createInFeedCpcQueryForPortfolium(feed, isPortfolium);
					} else {
						String queryString = createFixedRateCpcFeedQuery(feed);
						query = query + queryString;

						portfoliumQuery = portfoliumQuery + createFixedRateCpcFeedQueryForPortfolium(feed, isPortfolium);
					}
				} catch (Exception e) {
					// e.printStackTrace();
					// TODO Uncomment
					// sendExceptionEmail("High Alert Exception in jobs uploading",
					// "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>"
					// + e);
					// e.printStackTrace();
				}
			}
		}
		// This output will be read by shell script. Except this no output
		// should be given by this program.
		if (isPortfolium) {

			portfoliumQuery = portfoliumQuery.replaceAll("tbl_all_jobs", "tbl_job_export_portfolium_main");
			portfoliumQuery = portfoliumQuery.replaceAll("employer,title,description,postingdate,joburl,zipcode,city,state,source,gross_cpc,cpc,source_name,display_name",
					"employer,title,description,postingdate,joburl,zipcode,city,state,category,job_type,source,gross_cpc,cpc,source_name,display_name");
			System.out.println(portfoliumQuery);

		}

		else if (!isPortfolium) {
			System.out.println(query);
		}
	}

	public static DetermineFeedModel removeEmptyOrNullRateParameters(DetermineFeedModel feed) {

		try {
			if (feed.getSalesCommission1() == null || feed.getSalesCommission1().equals("")) {
				feed.setSalesCommission1("0");
			}
		} catch (Exception e) {
			feed.setSalesCommission1("0");
		}
		try {
			if (feed.getSalesCommission2() == null || feed.getSalesCommission2().equals("")) {
				feed.setSalesCommission2("0");
			}
		} catch (Exception e) {
			feed.setSalesCommission2("0");
		}
		try {
			if (feed.getGrossCpc() == null || feed.getGrossCpc().equals("")) {
				feed.setGrossCpc("0");
			}
		} catch (Exception e) {
			feed.setGrossCpc("0");
		}
		try {
			if (feed.getClickDiffAdjestment() == null || feed.getClickDiffAdjestment().equals("")) {
				feed.setClickDiffAdjestment("0");
			}
		} catch (Exception e) {
			feed.setClickDiffAdjestment("0");
		}
		try {
			if (feed.getDiscardedClicks() == null || feed.getDiscardedClicks().equals("")) {
				feed.setDiscardedClicks("0");
			}
		} catch (Exception e) {
			feed.setDiscardedClicks("0");
		}
		return feed;
	}

	// Create In Feed Cpc Query
	public static String createInFeedCpcQuery(DetermineFeedModel feed) {

		String salesComission1 = feed.getSalesCommission1();
		String salesComission2 = feed.getSalesCommission2();
		String clickDiiference = feed.getClickDiffAdjestment();
		String discardedClicks = feed.getDiscardedClicks();

		String queryString = tableMap.get(feed.getFeedName());

		if (feed.getFeedName().equals(viktreCareerFeedName)) {
			queryString = queryString.replace("LEFT(", "").replace(", 400)", "");
		}

		try {
			queryString = queryString.replace("DYNAMIC_SOURCE", feed.getFeedOrder());
		} catch (Exception e) {
			queryString = queryString.replace("DYNAMIC_SOURCE", "0.0");
		}
		queryString = queryString.replace("'DYNAMIC_GROSS_CPC'", "(cpc/100)");

		try {
			queryString = queryString.replace("'DYNAMIC_CPC'",
					"(cpc - (cpc * " + salesComission1 + "/100) - (cpc * " + salesComission2 + "/100) - (cpc * " + clickDiiference + "/100) - (cpc * " + discardedClicks + "/100))  / 100 ");
		} catch (Exception e) {
			queryString = queryString.replace("'DYNAMIC_CPC'",
					"(cpc - (cpc * " + "0" + "/100) - (cpc * " + "0" + "/100) - (cpc * " + "0" + "/100) - (cpc * " + "0" + "/100))  / 100 ");
		}

		return queryString;
	}

	// Create Fixed Rate Feed Cpc Query
	public static String createFixedRateCpcFeedQuery(DetermineFeedModel feed) {
		String queryString = tableMap.get(feed.getFeedName());

		if (feed.getFeedName().equals(viktreCareerFeedName)) {
			queryString = queryString.replace("LEFT(", "").replace(", 400)", "");
		}

		try {
			queryString = queryString.replace("DYNAMIC_SOURCE", feed.getFeedOrder());
			queryString = queryString.replace("DYNAMIC_GROSS_CPC", feed.getGrossCpc());
			queryString = queryString.replace("DYNAMIC_CPC", feed.getEffectiveCpc() + "");
			return queryString;

		} catch (Exception e) {
			// e.printStackTrace();
		}
		return "";
	}

	public static void sendExceptionEmail(String subject, String body) {

		SendGrid sendgrid;
		SendGrid.Email email;
		sendgrid = new SendGrid("fbg", "Sarasota99");
		email = new SendGrid.Email();
		email.setFrom("alerts@oakjobalerts.com");
		String[] bccList;
		email.setSubject(subject);
		email.addTo("gurpreet.s@signitysolutions.in");

		// bccList = new String[4];
		// bccList[0] = "rajinder@signitysolutions.com";
		// bccList[1] = "pawan@signitysolutions.co.in";
		// bccList[2] = "gagan@signitysolutions.com";
		// bccList[3] = "parveen.k@signitysolutions.co.in";
		// email.setBcc(bccList);

		email.setHtml(body);

		try {
			sendgrid.send(email);
			// System.out.println(body);
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	// Create In Feed Cpc Query
	public static String createInFeedCpcQueryForPortfolium(DetermineFeedModel feed, boolean isPortFolium) {

		String salesComission1 = feed.getSalesCommission1();
		String salesComission2 = feed.getSalesCommission2();
		String clickDiiference = feed.getClickDiffAdjestment();
		String discardedClicks = feed.getDiscardedClicks();

		String queryString = tableMap.get(feed.getFeedName());

		// Replace LEFT body condition in description
		queryString = queryString.replace("LEFT(", "").replace(", 400)", "");

		// category,job_type,'DYNAMIC_SOURCE'

		queryString = queryString.replace("'DYNAMIC_SOURCE'", "category,job_type,'DYNAMIC_SOURCE'");

		try {
			queryString = queryString.replace("DYNAMIC_SOURCE", "" + feed.getFeedOrder());
		} catch (Exception e) {
			queryString = queryString.replace("DYNAMIC_SOURCE", "0.0");
		}
		queryString = queryString.replace("'DYNAMIC_GROSS_CPC'", "(cpc/100)");

		try {
			queryString = queryString.replace("'DYNAMIC_CPC'",
					"(cpc - (cpc * " + salesComission1 + "/100) - (cpc * " + salesComission2 + "/100) - (cpc * " + clickDiiference + "/100) - (cpc * " + discardedClicks + "/100))  / 100 ");
		} catch (Exception e) {
			queryString = queryString.replace("'DYNAMIC_CPC'",
					"(cpc - (cpc * " + "0" + "/100) - (cpc * " + "0" + "/100) - (cpc * " + "0" + "/100) - (cpc * " + "0" + "/100))  / 100 ");
		}

		// if (isPortFolium) {
		// queryString = queryString.replace(";", "");
		// if (!feed.getFeedName().toLowerCase().contains("viktre")
		// && !feed.getFeedName().toLowerCase().contains("vcn single")) {
		//
		// if (queryString.toLowerCase().contains("where")) {
		//
		// queryString = queryString + " and cpc > 30 ;";
		// } else {
		// queryString = queryString + " where cpc > 30 ;";
		// }
		//
		// } else {
		// queryString = queryString + " ;";
		// }
		//
		// }

		return queryString;
	}

	// Create Fixed Rate Feed Cpc Query
	public static String createFixedRateCpcFeedQueryForPortfolium(DetermineFeedModel feed, boolean isPortFolium) {
		String queryString = tableMap.get(feed.getFeedName());

		// Replace LEFT body condition in description
		queryString = queryString.replace("LEFT(", "").replace(", 400)", "");

		try {

			queryString = queryString.replace("'DYNAMIC_SOURCE'", "category,job_type,'DYNAMIC_SOURCE'");

			queryString = queryString.replace("DYNAMIC_SOURCE", feed.getFeedOrder());
			queryString = queryString.replace("DYNAMIC_GROSS_CPC", feed.getGrossCpc());
			queryString = queryString.replace("DYNAMIC_CPC", feed.getEffectiveCpc() + "");

			// if (isPortFolium) {
			// queryString = queryString.replace(";", "");
			// if (!feed.getFeedName().toLowerCase().contains("viktre")
			// && !feed.getFeedName().toLowerCase().contains("vcn single")) {
			//
			// if (queryString.toLowerCase().contains("where")) {
			//
			// queryString = queryString + " and cpc > 30 ;";
			// } else {
			// queryString = queryString + " where cpc > 30 ;";
			// }
			//
			// } else {
			// queryString = queryString + " ;";
			// }
			//
			// }

			return queryString;

		} catch (Exception e) {
			// e.printStackTrace();
		}
		return "";
	}
}
