package com.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.model.DisplayPhpMapping;
import com.model.FeedOrderModel;
import com.model.FeedSourceQueriesModel;
import com.model.phpMappingClassModel;
import com.sendgrid.SendGrid;

public class DedupeMain {

    public static Map<String, String> tableMap;
    public static Map<String, String> sourceMappingMap;

     // Local test
     public static String masterFilePath = "/home/gurpreet/Desktop/FeedOrder/FeedOrderFiles/MasterFeedOrder.txt";
     public static String pathToWriteFile = "/home/gurpreet/Desktop/FeedOrder/masterSourceMapping/databaseSourceMappingFile.txt";
     public static String jsonPhpFilePath = "/home/gurpreet/Desktop/FeedOrder/masterSourceMapping/sourceMappingJson.json";
     public static String readSourceFeedQueryFilePath = "/home/gurpreet/Desktop/FeedOrder/masterSourceMapping/readFeedSourceQueries.txt";
     public static String readSourceDisplayMappingFilePath = "/home/gurpreet/Desktop/FeedOrder/masterSourceMapping/readSourceMappingJson.txt";

    // Server path
//    public static String masterFilePath = "/var/nfs-93/redirect/mis_logs/FeedOrderFiles/MasterFeedOrder.txt";
//    public static String pathToWriteFile = "/var/nfs-93/redirect/mis_logs/masterSourceMapping/databaseSourceMappingFile.txt";
//    public static String jsonPhpFilePath = "/var/nfs-93/redirect/mis_logs/masterSourceMapping/sourceMappingJson.json";
//    public static String readSourceFeedQueryFilePath = "/var/nfs-93/redirect/mis_logs/masterSourceMapping/readFeedSourceQueries.txt";
//    public static String readSourceDisplayMappingFilePath = "/var/nfs-93/redirect/mis_logs/masterSourceMapping/readSourceMappingJson.txt";

    public static void main(String[] args) {
        setUpTableMap();
        List<FeedOrderModel> feedOrderList = readOakFeedOrder();
        prepareQuery(feedOrderList);
        writeFilesOnServer(feedOrderList);
    }

    // This method will prepare dynamic queries by reading databaseSourceMappingFile and shell script will execute those queries.
    public static void prepareQuery(List<FeedOrderModel> feedOrderList) {
        String query = "";

        // Prepare feed with duplicate jobs removal active
        for (FeedOrderModel feed : feedOrderList) {
            if (feed.getActive().equals("1") && !feed.getFeedName().contains("Api") && feed.getRemoveDuplicate().equals("1")) {
                try {
                    query = query + tableMap.get(feed.getFeedName()).replace("DYNAMIC_SOURCE", feed.getSourceNumber());
                } catch (Exception e) {
                    sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
                    // e.printStackTrace();
                }
            }
        }
        // Alter table add unique index, and later remove unique index
        query = query + "ALTER IGNORE TABLE tbl_all_jobs ADD UNIQUE INDEX idx_name (employer, title, zipcode);"
                + "ALTER TABLE tbl_all_jobs drop INDEX idx_name;";

        for (FeedOrderModel feed : feedOrderList) {
            if (feed.getActive().equals("1") && !feed.getFeedName().contains("Api") && feed.getRemoveDuplicate().equals("0")) {
                try {
                    query = query + tableMap.get(feed.getFeedName()).replace("DYNAMIC_SOURCE", feed.getSourceNumber());
                } catch (Exception e) {
                    sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
                    // e.printStackTrace();
                }
            }
        }
        // This output will be read by shell script. Except this no output should be given by this program.
        System.out.println(query);
    }

    // This method will read MasterFeedOrder and prepare List of feeds to be used for writing queries
    public static List<FeedOrderModel> readOakFeedOrder() {
        List<FeedOrderModel> feedOrderList = new ArrayList<FeedOrderModel>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(masterFilePath));

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    FeedOrderModel feedOrder = new FeedOrderModel();
                    String[] splittedLine = line.split("\\|");

                    feedOrder.setFeedName(splittedLine[0].trim());
                    feedOrder.setActive(splittedLine[1].trim());
                    feedOrder.setSourceNumber(splittedLine[2].trim());
                    feedOrder.setRemoveDuplicate(splittedLine[4].trim());
                    if (!feedOrder.getFeedName().contains("Api") && feedOrder.getActive().equals("1")) {
                        feedOrderList.add(feedOrder);
                    }
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
                    // e.printStackTrace();
                }
            }
            bufferedReader.close();

        } catch (Exception e) {
            sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
            // e.printStackTrace();
        }

        return feedOrderList;
    }

    // This method will write sourceMappingJson.json and databaseSourceMappingFile.txt to be read by queue making and php team.
    public static void writeFilesOnServer(List<FeedOrderModel> feedOrderList) {

        List<String> source = new ArrayList<String>();
        List<String> sourceInsert = new ArrayList<String>();

        for (int i = 0; i <= 100; i++) {
            source.add("");
            sourceInsert.add("");
        }

        // Write File for Query mapping java program
        try {
            String fileWriteString = "";
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(pathToWriteFile));

            for (FeedOrderModel feed : feedOrderList) {
                fileWriteString = fileWriteString + feed.getFeedName() + "|" + feed.getSourceNumber() + "|" + sourceMappingMap.get(feed.getFeedName()) + "\n";
            }
            bufferedWriter.write(fileWriteString);
            bufferedWriter.close();
        } catch (Exception e) {
            sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
            // e.printStackTrace();
        }

        // Write JSON file to be read by Php Team
        source.add("");
        sourceInsert.add("");
        for (FeedOrderModel feed : feedOrderList) {
            try {
                if (sourceMappingMap.get(feed.getFeedName()).trim().equals("-1")) {
                    source.set(Integer.parseInt(feed.getSourceNumber()), "");
                } else
                    source.set(Integer.parseInt(feed.getSourceNumber()), sourceMappingMap.get(feed.getFeedName()).trim());
                sourceInsert.set(Integer.parseInt(feed.getSourceNumber()), feed.getFeedName().trim());

            } catch (Exception e) {
                sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
                // e.printStackTrace();
            }
        }

        DisplayPhpMapping displayMapiing = new DisplayPhpMapping();
        displayMapiing.setSource(source);
        displayMapiing.setSourceInsert(sourceInsert);
        String json = new Gson().toJson(displayMapiing);

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(jsonPhpFilePath));
            bufferedWriter.write(json);
            bufferedWriter.close();
        } catch (Exception e) {
            sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
            // e.printStackTrace();
        }

    }

    // This method will read readSourceMappingJson and readFeedSourceQueries for mapping and queries of feed order sources
    public static void setUpTableMap() {
        sourceMappingMap = new HashMap<String, String>();
        tableMap = new HashMap<String, String>();
        try {
            // Read mapping file
            BufferedReader bufferedReader = new BufferedReader(new FileReader(readSourceDisplayMappingFilePath));
            Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
            List<phpMappingClassModel> phpMappingList = new ArrayList<phpMappingClassModel>();
            phpMappingList = gson.fromJson(bufferedReader, new TypeToken<List<phpMappingClassModel>>() {
            }.getType());
            bufferedReader.close();
            for (phpMappingClassModel phpMapping : phpMappingList) {
                sourceMappingMap.put(phpMapping.getSource(), phpMapping.getSourceInsert());
            }

            // Read queries files
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader(readSourceFeedQueryFilePath));
            List<FeedSourceQueriesModel> feedSourceQueryList = new ArrayList<FeedSourceQueriesModel>();
            feedSourceQueryList = gson.fromJson(bufferedReader2, new TypeToken<List<FeedSourceQueriesModel>>() {
            }.getType());
            bufferedReader2.close();
            for (FeedSourceQueriesModel sourceQuery : feedSourceQueryList) {
                tableMap.put(sourceQuery.getFeedName(), sourceQuery.getQuery());
            }
        } catch (Exception e) {
            sendExceptionEmail("High Alert Exception in jobs uploading", "Hi Following Exception occured during jobs uploading.<br><br> <a href='<UNSUBSCRIBE>' target='_blank'></a>" + e);
            // e.printStackTrace();
        }

    }

    public static void sendExceptionEmail(String subject, String body) {

        SendGrid sendgrid;
        SendGrid.Email email;
        sendgrid = new SendGrid("fbg", "Sarasota99");
        email = new SendGrid.Email();
        email.setFrom("alerts@oakjobalerts.com");
        String[] bccList;
        email.setSubject(subject);
        email.addTo("gurpreet.s@signitysolutions.in");

//        bccList = new String[4];
//        bccList[0] = "rajinder@signitysolutions.com";
//        bccList[1] = "pawan@signitysolutions.co.in";
//        bccList[2] = "gagan@signitysolutions.com";
//        bccList[3] = "parveen.k@signitysolutions.co.in";
//        email.setBcc(bccList);

        email.setHtml(body);

        try {
            sendgrid.send(email);
            // System.out.println(body);
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

}
